# CalculatorConsole

## English

This is a simple console calculator program written in .NET 8 using Visual Studio Code. The program performs basic addition, subtraction, multiplication and division operations with two numbers entered by the user from the console.

### Use

**1.** Clone this repository to your local machine.

**2.** Open the project in Visual Studio Code.

**3.** In the built-in Visual Studio Code terminal, navigate to the project directory.

**4.** Run the program using the dotnet run command.

**5.** Follow the instructions on the console to enter a math operation. To reset the entered values, press Ctrl+x; To exit the application, press the Escape key.

**6.** After completing the first math operation, you will receive the previous instructions again. It is important to note that when you press any key, the result obtained in the operation will be used as the first value of the new mathematical operation. This process will be repeated, allowing you to work with the results obtained without having to re-enter them.

### Example
`****************************************************** ********`

`\\\\\\\\\\\\\\\\\\\\ Calculator Console //////////////////`

`****************************************************** ********`

Press any key to continue.

Press Ctrl+x to restart values.

Press the Escape key (Esc) to exit.

`****************************************************** *********`

Press first value:

5

Press second value:

9

Press operation math + - * / : *

`-------------------------------------------`

Result: 45

`-------------------------------------------`

### Contribution

Your participation is welcomed and valued in this project! Here are some ways you can contribute:

- **Report problems:** If you find any problem or error, please clearly comment on the problem found.
  
- **Request features:** Do you have a great idea for a new feature? We'd love to hear it! label it "feature request".
  
- **Submit pull requests:** Would you like to contribute code? If you've fixed a problem, implemented a new feature, or improved the code in some way, submit a pull request! Be sure to follow the contribution guidelines and carefully describe the changes you made.

- **Provide feedback:** Do you have any comments or suggestions to improve the project? We'd love to hear it! Feel free to leave your comments on existing issues or open a new one to discuss your ideas.

Thank you for your interest in contributing to the project! Together we can do even better.

### License

This project is under the [MIT License](LICENSE).

## Español

Este es un programa simple de calculadora de consola escrito en .NET 8 utilizando Visual Studio Code. El programa realiza operaciones básicas de suma, resta, multiplicación y división con dos números ingresados por el usuario desde la consola.

### Uso

**1.** Clona este repositorio en tu máquina local.

**2.** Abre el proyecto en Visual Studio Code.

**3.** En la terminal integrada de Visual Studio Code, navega al directorio del proyecto.

**4.** Ejecuta el programa utilizando el comando dotnet run.

**5.** Sigue las instrucciones en la consola para ingresar una operación matemática. Para reiniciar los valores ingresados, presiona **Ctrl+x**; para salir de la aplicación, presiona la tecla **Escape**.

**6.** Después de completar la primera operación matemática, recibirás nuevamente las instrucciones anteriores. Es important destacar que al presionar cualquier tecla, el resultado obtenido en la operación se utilizará como el primer valor de la nueva operación matemática. Este proceso se repetirá, permitiéndote trabajar con los resultados obtenidos sin necesidad de volver a ingresarlos.

### Ejemplo
`*********************************************************`

`\\\\\\\\\\\\\\\\\\ Calculator Console //////////////////`

`*********************************************************`

Press any key for continue.

Press Ctrl+x to restart values.

Press the Escape key (Esc) to exit.

`**********************************************************`

Press first value:

5

Press second value:

9

Press operation math + - * / : *

`----------------------------------------------`

Result: 45

`----------------------------------------------`

### Contribución

¡Tu participación es bienvenida y valorada en este proyecto! Aquí hay algunas formas en las que puedes contribuir:

- **Reporta problemas:** Si encuentras algún problema o error, por favor, comentalo claramente el problema encontrado.
  
- **Solicita características:** ¿Tienes alguna idea genial para una nueva característica? ¡Nos encantaría escucharla! etiquétalo como "solicitud de característica".
  
- **Envía solicitudes de extracción:** ¿Te gustaría contribuir con código? Si has solucionado un problema, implementado una nueva característica o mejorado el código de alguna manera, ¡envía una solicitud de extracción! Asegúrate de seguir las pautas de contribución y describir detalladamente los cambios realizados.

- **Proporciona comentarios:** ¿Tienes algún comentario o sugerencia para mejorar el proyecto? ¡Nos encantaría escucharlo! Siéntete libre de dejar tus comentarios en los problemas existentes o abrir uno nuevo para discutir tus ideas.

¡Gracias por tu interés en contribuir al proyecto! Juntos podemos hacerlo aún mejor.

### Licencia

Este proyecto está bajo la [Licencia MIT](LICENSE).
