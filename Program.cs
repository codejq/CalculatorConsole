﻿using System;

namespace nsCalculatorConsole
{

    /// <summary>
    /// Calculator Console
    /// Program that allow to make basic math operation like: 
    ///     + Addition
    ///     - Subtraction
    ///     * Multiplication
    ///     / Division
    /// There are options like reset values or exit program.
    /// </summary>
    internal class Program
    {

        static void Main(string[] args)
        {

            string? firstValue = string.Empty;
            string? secondValue = string.Empty;
            string? operationValue = string.Empty;
            decimal? resultValue = 0;
            bool initValues = true;

            ConsoleKeyInfo consoleAction;
            Console.WriteLine("*********************************************************");
            Console.WriteLine(@"\\\\\\\\\\\\\\\\\\ Calculator Console //////////////////");
            Console.WriteLine("*********************************************************");
            showMsgInformationConsole();
            Console.WriteLine("**********************************************************\n");
            ConsoleKeyInfo keyInfo;
            do
            {
                // First operation for calculator console.
                if (initValues)
                {
                    Console.WriteLine("Press first value: ");
                    firstValue = Console.ReadLine();
                    Console.WriteLine("Press second value: ");
                    secondValue = Console.ReadLine();
                    initValues = false;
                }
                else
                {
                    // Other operation based in previous operation result.
                    firstValue = Convert.ToString(resultValue);
                    Console.WriteLine("First value: {0}", firstValue);
                    Console.WriteLine("Press second value: ");
                    secondValue = Console.ReadLine();
                }

                if (getOperationRequest(firstValue, secondValue, out resultValue))
                {
                    Console.WriteLine("----------------------------------------------");
                    Console.WriteLine("Result: {0}", Convert.ToString(resultValue));
                    Console.WriteLine("----------------------------------------------");
                    Console.WriteLine();
                }

                // Show options in program
                showMsgInformationConsole();

                keyInfo = Console.ReadKey(intercept: true);

                Console.WriteLine();
                Console.WriteLine(@"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
                Console.WriteLine();

                if (keyInfo.Modifiers != 0 && keyInfo.Key == ConsoleKey.X)
                {
                    Console.WriteLine("Reseting values .. ");
                    Console.WriteLine();
                    initValues = true;
                }

            } while (keyInfo.Key != ConsoleKey.Escape);

            Console.WriteLine("Program finished successfully!!");

        }

        /// <summary>
        /// Shows information of commands allowed in program.
        /// </summary>
        private static void showMsgInformationConsole()
        {
            Console.WriteLine("Press any key for continue.");
            Console.WriteLine("Press Ctrl+x to restart values.");
            Console.WriteLine("Press the Escape key (Esc) to exit.");
        }

        /// <summary>
        /// Request the math operation. e.g. + - * /
        /// </summary>
        private static bool getOperationRequest(in string? firstValue, in string? secondValue, out decimal? resultValue)
        {
            resultValue = null;
            try
            {
                Console.Write("Press operation math + - * / : ");
                string? operationValue = Console.ReadLine();

                // Calculating result
                switch (operationValue)
                {
                    case "+":
                        resultValue = Convert.ToDecimal(firstValue) + Convert.ToDecimal(secondValue);
                        break;
                    case "-":
                        resultValue = Convert.ToDecimal(firstValue) - Convert.ToDecimal(secondValue);
                        break;
                    case "*":
                        resultValue = Convert.ToDecimal(firstValue) * Convert.ToDecimal(secondValue);
                        break;
                    case "/":
                        resultValue = Convert.ToDecimal(firstValue) / Convert.ToDecimal(secondValue);
                        break;
                    default:
                        Console.WriteLine("Invalid Character, press one symbol allowed again.");
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}


